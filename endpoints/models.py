from django.db import models 


qa='qa'
faq='faq'
glossary='glossary'

bot_skills = [
	(qa,'qa'),
	(faq,'faq'),
	(glossary,'glossary')
]



class AskBot(models.Model):
	question = models.CharField(max_length=120)
	bot = models.CharField(max_length=20,choices=bot_skills,default=qa)
	answer = models.CharField(max_length=120,blank=True,null=True)

	

