from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import AskBot
from .serializers import AskBotSerializers
from rest_framework import viewsets
from qary.clibot import CLIBot
from qary.skills import qa_bots

qa_bot = qa_bots.Bot()




class AskBotViewSet(viewsets.ModelViewSet):
    queryset = AskBot.objects.all()
    serializer_class = AskBotSerializers

    def perform_create(self, serializer):
        if self.request.data['bot']=='glossary':
            serializer.save(answer=CLIBot(bots='glossary'.split(','), num_top_replies=1).reply(self.request.data['question']))
        elif self.request.data['bot']=='faq':
            serializer.save(answer=CLIBot(bots='faq'.split(','), num_top_replies=1).reply(self.request.data['question']))
        else:
            # serializer.save(answer=CLIBot(bots='qa'.split(','), num_top_replies=1).reply(self.request.data['question']))
             serializer.save(answer=qa_bot.reply(self.request.data['question'])[0][1])





